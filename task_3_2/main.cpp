#include <iostream>
#include <vector>
#include <cassert>
#include <functional>
#include <queue>

#define SET 1
#define EMPTY 0
#define INF -1

struct IGraph {
    virtual ~IGraph() = default;

    virtual void AddEdge(int from, int to) = 0;
    virtual int VerticesCount() const  = 0;

    virtual std::vector<int> GetNextVertices(int vertex) const = 0;
    virtual std::vector<int> GetPrevVertices(int vertex) const = 0;
};

class ListGraph : public IGraph {
public:
    explicit ListGraph(int size) : adjacencyLists(size)
    {
    }

    explicit ListGraph(const IGraph &graph) : adjacencyLists(graph.VerticesCount())
    {
        for (int i = 0; i < graph.VerticesCount(); i++)
        {
            adjacencyLists[i] = graph.GetNextVertices(i);
        }
    }

    ~ListGraph() override = default;

    void AddEdge(int from, int to) override
    {
        assert(0 <= from && from < adjacencyLists.size());
        assert(0 <= to && to < adjacencyLists.size());
        adjacencyLists[from].push_back(to);
    }

    int VerticesCount() const override
    {
        return (int)adjacencyLists.size();
    }

    std::vector<int> GetNextVertices(int vertex) const override
    {
        assert(0 <= vertex && vertex < adjacencyLists.size());
        return adjacencyLists[vertex];
    }

    std::vector<int> GetPrevVertices(int vertex) const override
    {
        assert(0 <= vertex && vertex < adjacencyLists.size());
        std::vector<int> prevVertices;

        for (int from = 0; from < (int)adjacencyLists.size(); from++)
        {
            for (int to: adjacencyLists[from])
            {
                if (to == vertex)
                {
                    prevVertices.push_back(from);
                }
            }
        }
        return prevVertices;
    }

    void printVectorList(){
        for(int i = 0; i < adjacencyLists.size(); i++)
        {
            std::cout << i << "|" << " ";
            for(int j = 0; j < adjacencyLists[i].size(); j++)
            {
                std::cout << adjacencyLists[i][j] << " ";
            }
            std::cout << std::endl;
        }
        return;
    }


    int CountMinWay(int from, int to)
    {
        std::vector<int> shortDistance(adjacencyLists.size(), INF); // вектор кратчайших расстояний
        shortDistance[from] = 0;
        std::vector<int> k(adjacencyLists.size(), 0); // вектор с числом кратчайших путей
        k[from] = 1;
        std::vector<bool> visited(adjacencyLists.size(), false);
        visited[from] = true;


        std::queue<int> queue;
        queue.push(from);

        int currentVertex;
        std::vector<int> nextVertices;
        while(!queue.empty())
        {
            currentVertex = queue.front();
            queue.pop();
            nextVertices = this->GetNextVertices(currentVertex);
            for(int n: nextVertices)
            {
                if(shortDistance[n] == INF)
                {
                    shortDistance[n] = shortDistance[currentVertex] + 1;
                    visited[n] = true;
                    queue.push(n);
                }
                if(shortDistance[n] > shortDistance[currentVertex] + 1)
                {
                    shortDistance[n] = shortDistance[currentVertex] + 1;
                    k[n] = 1;
                }
                if(shortDistance[n] == shortDistance[currentVertex] + 1)
                    k[n] += k[currentVertex];
            }
        }
        return k[to];
    }


private:
    std::vector<std::vector<int>> adjacencyLists;
};

void BFS(const IGraph &graph, int vertex, std::vector<bool> &visited, const std::function<void(int)>& func)
{
    std::queue<int> qu;
    qu.push(vertex);
    visited[vertex] = true;

    while(!qu.empty())
    {
        int currentVertex = qu.front();
        qu.pop();

        func(currentVertex);

        for(int nextVertex: graph.GetNextVertices(currentVertex))
        {
            if(!visited[nextVertex])
            {
                visited[nextVertex] = true;
                qu.push(nextVertex);
            }
        }
    }
}

void mainBFS(const IGraph &graph, const std::function<void(int)>& func)
{
    std::vector<bool> visited(graph.VerticesCount(), false);

    for(int i = 0; i < graph.VerticesCount(); i++)
    {
        if(!visited[i])
            BFS(graph, i, visited, func);
    }
}

int main() {
    int countVertices;
    std::cin >> countVertices;
    ListGraph graph(countVertices);
    int countEdges;
    std::cin >> countEdges;
    int from, to;
    for (int i = 0; i < countEdges; i++)
    {
        std::cin >> from >> to;
        graph.AddEdge(from, to);
        graph.AddEdge(to, from);
    }

    //graph.printVectorList();    //mainBFS(graph, [](int vertex){ std::cout << vertex << " ";});
    std::cin >> from >> to;
    std::cout << graph.CountMinWay(from, to) << std::endl;

    return 0;
}