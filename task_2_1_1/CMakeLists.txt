cmake_minimum_required(VERSION 3.15)
project(task_2_1_1)

set(CMAKE_CXX_STANDARD 17)

add_executable(task_2_1_1 main.cpp)