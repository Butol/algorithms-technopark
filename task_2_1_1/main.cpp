#include <iostream>
#include <vector>

#define DEL "DEL"
#define NIL "NIL"

const size_t DEFAULT_SIZE = 8;

class StringHasher {
public:
    StringHasher(size_t prime = 71) : prime(prime) {}

    size_t operator()(const std::string &str, size_t size) {
        size_t hash = 0;
        for (char i : str) {
            hash = (hash * prime + i);
        }

        return hash % size;
    }
private:
    size_t prime;
};

template <typename T, typename Hasher>
class HashTable {
public:
    explicit HashTable(size_t initial_size = DEFAULT_SIZE) : size(0), table(initial_size, NIL) {};

    bool Add(const T& key) {
        if(size * 4 > table.size() * 3) {
            Grow();
        }
        
        size_t hash = hasher(key, table.size());

        if(table[hash] == key)
            return false;

        if(table[hash] == NIL) {
            table[hash] = key;
            size++;
            return true;
        }

        size_t delIndex = 0;
        bool flag = false;
        if (table[hash] == DEL) {
            delIndex = hash;
            flag = true;
        }

        size_t prob = hash;
        for (size_t i = 0; i < table.size(); i++) {
            prob = Probing(prob, i);
            if(table[prob] == key)
                return false;
            if(table[prob] == DEL && !flag) {
                delIndex = prob;
                flag = true;
            }
            if(table[prob] == NIL && !flag)
            {
                table[prob] = key;
                size++;
                return true;
            }
        }

        table[delIndex] = key;
        size++;
        return true;
    }

    bool Delete(const T& key) {

        size_t hash = hasher(key, table.size());

        if(table[hash] == key) {
            table[hash] = DEL;
            size--;
            return true;
        }

        if(table[hash] == NIL)
            return false;

        size_t prob = hash;
        for (size_t i = 0; i < table.size(); i++) {
            prob = Probing(prob, i);
            if(table[prob] == key) {
                table[prob] = DEL;
                size--;
                return true;
            }
            if(table[prob] == NIL) {
                return false;
            }
        }

        return false;
    }

    bool Has(const T& key) {
        size_t hash = hasher(key, table.size());

        if(table[hash] == key)
            return true;

        if(table[hash] == NIL)
            return false;

        size_t prob = hash;
        for (size_t i = 0; i < table.size(); i++) {
            prob = Probing(prob, i);
            if(table[prob] == key)
                return true;
            if(table[prob] == NIL)
                return false;
        }

        return false;
    }
private:
    void Grow() {
        HashTable<std::string, StringHasher> newTable(table.size() * 2);
        for(size_t i = 0; i < table.size(); i++) {
            if(table[i] != NIL && table[i] != DEL) {
                newTable.Add(table[i]);
            }
        }

        *this = newTable;
    }

    size_t Probing(size_t hash, size_t i) {
        return (hash + i + 1) % table.size();
    }

    Hasher hasher;
    size_t size;
    std::vector<T> table;
};

int main() {
    HashTable<std::string, StringHasher> table;

    char op;
    std::string str;

    while (std::cin >> op >> str)
    {
        switch (op)
        {
            case '?':
            {
                std::cout << (table.Has(str) ? "OK" : "FAIL") << std::endl;
                break;
            }
            case '+':
            {
                std::cout << (table.Add(str) ? "OK" : "FAIL") << std::endl;
                break;
            }
            case '-':
            {
                std::cout << (table.Delete(str) ? "OK" : "FAIL") << std::endl;
                break;
            }
        }
    }

    return 0;
}
