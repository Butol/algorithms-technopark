#include <iostream>
#include <vector>
#include <cassert>
#include <functional>
#include <queue>
#include <unordered_set>

#define SET 1
#define EMPTY 0

struct IGraph {
    virtual ~IGraph() = default;

    virtual void AddEdge(int from, int to) = 0;
    virtual int VerticesCount() const  = 0;

    virtual std::vector<int> GetNextVertices(int vertex) const = 0;
    virtual std::vector<int> GetPrevVertices(int vertex) const = 0;
};

class ListGraph : public IGraph {
public:
    explicit ListGraph(int size) : adjacencyLists(size)
    {
    }

    explicit ListGraph(const IGraph &graph) : adjacencyLists(graph.VerticesCount())
    {
        for (int i = 0; i < graph.VerticesCount(); i++)
        {
            adjacencyLists[i] = graph.GetNextVertices(i);
        }
    }

    ~ListGraph() override = default;

    void AddEdge(int from, int to) override
    {
        assert(0 <= from && from < adjacencyLists.size());
        assert(0 <= to && to < adjacencyLists.size());
        adjacencyLists[from].push_back(to);
    }

    int VerticesCount() const override
    {
        return (int)adjacencyLists.size();
    }

    std::vector<int> GetNextVertices(int vertex) const override
    {
        assert(0 <= vertex && vertex < adjacencyLists.size());
        return adjacencyLists[vertex];
    }

    std::vector<int> GetPrevVertices(int vertex) const override
    {
        assert(0 <= vertex && vertex < adjacencyLists.size());
        std::vector<int> prevVertices;

        for (int from = 0; from < (int)adjacencyLists.size(); from++)
        {
            for (int to: adjacencyLists[from])
            {
                if (to == vertex)
                {
                    prevVertices.push_back(from);
                }
            }
        }
        return prevVertices;
    }

private:
    std::vector<std::vector<int>> adjacencyLists;
};
class MatrixGraph : public IGraph {
public:
    explicit MatrixGraph(int size): adjacencyMatrix(size, std::vector<int>(size, EMPTY)), verticesCount(size) {}

    explicit MatrixGraph(const IGraph &graph){
        verticesCount = graph.VerticesCount();
        adjacencyMatrix.resize(verticesCount);
        std::vector<int> tmpNextVertices;
        for(int i = 0; i < verticesCount; i++)
        {
            adjacencyMatrix[i].resize(verticesCount);
            tmpNextVertices = graph.GetNextVertices(i);
            for (int vertex: tmpNextVertices)
            {
                adjacencyMatrix[i][vertex] = SET;
            }
        }
    }

    ~MatrixGraph() override= default;

    void AddEdge(int from, int to) override
    {
        assert(0 <= from && from < verticesCount);
        assert(0 <= to && to < verticesCount);

        adjacencyMatrix[from][to] = SET;
    }

    int VerticesCount() const override
    {
        return verticesCount;
    }

    std::vector<int> GetNextVertices(int vertex) const override
    {
        assert(0 <= vertex && vertex < verticesCount);
        std::vector<int> nextVertices;
        for(int currentVertex: adjacencyMatrix[vertex])
            if(currentVertex == SET)
                nextVertices.push_back(currentVertex);
        return nextVertices;
    }

    std::vector<int> GetPrevVertices(int vertex) const override
    {
        assert(0 <= vertex && vertex < verticesCount);
        std::vector<int> prevVertices;
        for(int i = 0; i < verticesCount; i++)
        {
            if(adjacencyMatrix[i][vertex] == SET)
                prevVertices.push_back(i);
        }
        return prevVertices;
    }

private:
    std::vector<std::vector<int>> adjacencyMatrix;
    int verticesCount;
};
class SetGraph : public IGraph {
public:
    explicit SetGraph(int size): vectorHashTable(size) {}
    explicit SetGraph(const IGraph &graph)
    {
        int countVertex = graph.VerticesCount();
        vectorHashTable.resize(countVertex);
        std::vector<int> tmpNextVertices;

        for(int i = 0; i < countVertex; i++)
        {
            tmpNextVertices = graph.GetNextVertices(i);

            for(int vertex: tmpNextVertices)
            {
                vectorHashTable[i].insert(vertex);
            }
        }
    }
    ~SetGraph() override = default;

    int VerticesCount() const override
    {
        return vectorHashTable.size();
    }

    void AddEdge(int from, int to) override
    {
        assert(0 <= from && from < vectorHashTable.size());
        assert(0 <= to && to < vectorHashTable.size());

        vectorHashTable[from].insert(to);
    }

    std::vector<int> GetNextVertices(int vertex) const override
    {
        assert(0 <= vertex && vertex < vectorHashTable.size());
        std::vector<int> nextVertices;

        for(int currentVertex: vectorHashTable[vertex])
            nextVertices.push_back(currentVertex);
        return nextVertices;
    }

    std::vector<int> GetPrevVertices(int vertex) const override
    {
        assert(0 <= vertex && vertex < vectorHashTable.size());
        std::vector<int> prevVertices;

        for(const auto & i : vectorHashTable)
        {
            if (i.find(vertex) != i.end())
                prevVertices.push_back(vertex);
        }
        return prevVertices;
    }

private:
    std::vector<std::unordered_set<int>> vectorHashTable;
};
class ArcGraph : public IGraph {
public:
    explicit ArcGraph(int size): verticesCount(size) {}
    explicit ArcGraph(const IGraph &graph)
    {
        verticesCount = graph.VerticesCount();
        std::vector<int> nextVertices;
        for (int i = 0; i < graph.VerticesCount(); i++)
        {
            nextVertices = graph.GetNextVertices(i);
            for(int vertex: nextVertices)
            {
                vectorPairs.emplace_back(i, vertex);
            }
        }
    }
    ~ArcGraph() override = default;;

    int VerticesCount() const override {
        return verticesCount;
    }

    void AddEdge(int from, int to) override
    {
        assert(0 <= from && from < verticesCount);
        assert(0 <= to && to < verticesCount);
        vectorPairs.emplace_back(from, to);
    }

    std::vector<int> GetNextVertices(int vertex) const override
    {
        assert(0 <= vertex && vertex < verticesCount);
        std::vector<int> nextVertices;

        for(const auto & vectorPair : vectorPairs)
        {
            if(vectorPair.first == vertex)
                nextVertices.push_back(vectorPair.second);
        }
        return  nextVertices;
    }

    std::vector<int> GetPrevVertices(int vertex) const override
    {
        assert(0 <= vertex && vertex < verticesCount);
        std::vector<int> prevVertices;

        for(const auto & vectorPair : vectorPairs)
        {
            if(vectorPair.second == vertex)
                prevVertices.push_back(vectorPair.second);
        }
        return  prevVertices;

    }
private:
    std::vector<std::pair<int,int>> vectorPairs;
    int verticesCount;
};

void BFS(const IGraph &graph, int vertex, std::vector<bool> &visited, const std::function<void(int)>& func)
{
    std::queue<int> qu;
    qu.push(vertex);
    visited[vertex] = true;

    while(!qu.empty())
    {
        int currentVertex = qu.front();
        qu.pop();

        func(currentVertex);

        for(int nextVertex: graph.GetNextVertices(currentVertex))
        {
            if(!visited[nextVertex])
            {
                visited[nextVertex] = true;
                qu.push(nextVertex);
            }
        }
    }
}
void mainBFS(const IGraph &graph, const std::function<void(int)>& func)
{
        std::vector<bool> visited(graph.VerticesCount(), false);

        for(int i = 0; i < graph.VerticesCount(); i++)
        {
           if(!visited[i])
               BFS(graph, i, visited, func);
        }
}

void DFS(const IGraph &graph, int vertex, std::vector<bool> &visited, const std::function<void(int)>& func)
{
    visited[vertex] = true;
    func(vertex);

    for(int nextVertex: graph.GetNextVertices(vertex))
    {
        if(!visited[nextVertex])
            DFS(graph, nextVertex, visited, func);
    }
}
void mainDFS(const IGraph &graph, const std::function<void(int)>& func)
{
    std::vector<bool> visited(graph.VerticesCount(), false);

    for(int i = 0; i < graph.VerticesCount(); i++)
    {
        if(!visited[i])
            DFS(graph, i, visited, func);
    }
}

int main() {
    ListGraph graph1(7);
    graph1.AddEdge(0,1);
    graph1.AddEdge(0,5);
    graph1.AddEdge(1,2);
    graph1.AddEdge(1,3);
    graph1.AddEdge(1,5);
    graph1.AddEdge(1,6);
    graph1.AddEdge(3,2);
    graph1.AddEdge(3,4);
    graph1.AddEdge(3,6);
    graph1.AddEdge(5,4);
    graph1.AddEdge(5,6);
    graph1.AddEdge(6,4);
    std::cout << graph1.VerticesCount() << std::endl;
    graph1.AddEdge(0,1);

    MatrixGraph graph2(graph1);
    std::cout << graph2.VerticesCount() << std::endl;
    graph1.AddEdge(0,1);

    MatrixGraph graph3(graph1);
    std::cout << graph3.VerticesCount() << std::endl;
    graph1.AddEdge(0,1);

    MatrixGraph graph4(graph1);
    std::cout << graph4.VerticesCount() << std::endl;
    graph1.AddEdge(0,1);

    return 0;
}