#include <iostream>
#include <cassert>
#include <utility>
#include <algorithm>
#include <vector>

#define DEFAULT_INITIAL_SIZE 4

template<class T>
class LessFunctor {
public:
    bool operator() (const T& left, const T& right) {
        return (left < right);
    }
};

template <class T, class Compare>
class Heap {
public:
    Heap(const Compare& cmp = Compare());
    ~Heap() { delete[] buffer; };
    T PeekMax() const;
    bool IsEmpty() const;
    bool IsFull() const;
    void Insert(int element);
    T ExtractMax();
    void printHeap();
    int Size() {return length;}

private:
    T* buffer;
    int length;
    int capacity;
    Compare cmp_func;

    void siftDown( int index );
    void siftUp( int i );
    void resize();
};

template <class T, class Compare>
Heap<T, Compare>::Heap(const Compare& cmp) {
    buffer = new T[DEFAULT_INITIAL_SIZE];
    length = 0;
    capacity = DEFAULT_INITIAL_SIZE;
    //cmp_func = cmp;
}

template <class T, class Compare>
bool Heap<T, Compare>::IsEmpty() const {
    return length == 0;
}
template <class T, class Compare>
T Heap<T, Compare>::PeekMax() const {
    assert(!IsEmpty());
    return buffer[0];
}
template <class T, class Compare>
void Heap<T, Compare>::siftDown(int index) {
    int left = 2 * index + 1;
    int right = 2 * index + 2;
    int largest = index;
    if( left < length && cmp_func(buffer[index], buffer[left]))
        largest = left;
    if( right < length && cmp_func(buffer[largest], buffer[right]))
        largest = right;
    if(largest != index ) {
        std::swap(buffer[index], buffer[largest]);
        siftDown( largest );
    }
}
template <class T, class Compare>
void Heap<T, Compare>::siftUp( int index ) {
    while( index > 0 ) {
        int parent = ( index - 1 ) / 2;
        if(buffer[index] <= buffer[parent])
            return;
        std::swap(buffer[index], buffer[parent] );
        index = parent;
    }
}
template <class T, class Compare>
void Heap<T, Compare>::Insert(int element) {
    if (IsFull())
        resize();

    length++;
    buffer[length - 1] = element;
    siftUp(length - 1);
}
template <class T, class Compare>
bool Heap<T, Compare>::IsFull() const {
    return length == capacity;
}
template <class T, class Compare>
void Heap<T, Compare>::resize() {
    int newBufferSize = std::max(capacity * 2, DEFAULT_INITIAL_SIZE);
    int* newBuffer = new int[newBufferSize];
    std::copy(buffer, buffer + length, newBuffer);
    delete[] buffer;
    buffer = newBuffer;
    capacity = newBufferSize;
}
template <class T, class Compare>
T Heap<T, Compare>::ExtractMax() {
    assert( !IsEmpty() );
    T result = buffer[0];
    buffer[0] = buffer[--length];
    if( !IsEmpty() ) {
        siftDown(0);
    }
    return result;
}
template <class T, class Compare>
void Heap<T, Compare>::printHeap() {
    for(int i = 0; i < length; i++) {
        std::cout << buffer[i] << " ";
    }
    std::cout << std::endl;
}

int main() {

    Heap<int, LessFunctor<int>> heap;
    int countFruits = 0;
    std::cin >> countFruits;

    for(int i = 0; i < countFruits; i++) {
        int weightFruit = 0;
        std::cin >> weightFruit;
        heap.Insert(weightFruit);
    }

    // Вес который может поднять мальчик
    int loadCapacity = 0;
    std::cin >> loadCapacity;

    int count = 0; // Количество шагов

    // Вес который сейчас держит Мальчик
    int currentCapacity = 0;
    // Вес текущего фрукта
    int currentWeightFruit = 0;

    // Динамический массив для вставки элементов обратно в хип
    std::vector<int> buffer;

    while(true) {

        if(!heap.IsEmpty())
            currentWeightFruit = heap.PeekMax();
        else
            currentWeightFruit = 0;

        while ((currentCapacity + currentWeightFruit) <= loadCapacity) {
            if(!heap.IsEmpty())
                currentWeightFruit = heap.ExtractMax();
            currentCapacity += currentWeightFruit;
            if(currentWeightFruit > 1)
                buffer.push_back(currentWeightFruit / 2);
            if(!heap.IsEmpty())
                currentWeightFruit = heap.PeekMax();
            if(heap.IsEmpty())
                break;
        }

        currentCapacity = 0;

        while(!buffer.empty()) {
            heap.Insert(buffer.back());
            buffer.pop_back();
        }

        count++;

        if(heap.IsEmpty())
            break;
    }

    std::cout << count;

    return 0;
}