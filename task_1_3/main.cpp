#include <iostream>

#define EMPTY_DEQUE -1
#define DEQUE_ERROR -2;
#define USER_ERROR -3;

class Deque {
public:
    explicit Deque(int countBuffer) : head(0), tail(0), capacity(countBuffer) {
        buffer = new int[countBuffer];
    }
    ~Deque() { delete[] buffer; }

    bool isEmpty() {
        return head == tail;
    }

    bool isFull() {
        return (head == ((tail + 1) % capacity));
    }

    void reallocBuffer() {
        int* newBuffer = new int[capacity * 2];
        for(int i = 0 ; i < capacity - 1; i++)
        {
            newBuffer[i] = buffer[head];
            head = (head + 1) % capacity;
        }
        delete[] buffer;
        buffer = newBuffer;
        head = 0;
        tail = capacity - 1;
        capacity *= 2;
    }

    void pushBack(int value) {
        if (isFull())
            reallocBuffer();

        buffer[tail] = value;
        tail = (tail + 1) % capacity;
    }

    void pushFront(int value) {
        if (isFull())
            reallocBuffer();

        head = (head - 1 + capacity) % capacity;
        buffer[head] = value;
   }

    int popBack() {
        if (isEmpty())
            return EMPTY_DEQUE;

        tail = (tail - 1 + capacity) % capacity;
        return buffer[tail];
    }

    int popFront() {
        if (isEmpty())
            return EMPTY_DEQUE;

        int ret = buffer[head];
        head = (head + 1) % capacity;
        return ret;
    }

private:
    int head;
    int tail;
    int capacity;
    int* buffer;
};

int main() {
    int operationCode = 0;  // Код операции
    int expectValue = 0; // Значение для Push и ожидаемое значение для Pop
    int result = 0; // Полученное значение
    int countOperation = 0; // Количество операций
    Deque deque(3);

    std::cin >> countOperation;

    for(int i = 0; i < countOperation; i++) {
        std::cin >> operationCode >> expectValue;

        if(operationCode == 1) {
            deque.pushFront(expectValue);
        }
        else if(operationCode == 2) {
            result = deque.popFront();
            if(result != expectValue)
            {
                std::cout << "NO" << std::endl;
                return 0;
            }
        }
        else if(operationCode == 3) {
            deque.pushBack(expectValue);
        }
        else if(operationCode == 4) {
            result = deque.popBack();
            if(result != expectValue)
            {
                std::cout << "NO" << std::endl;
                return 0;
            }
        }
        else {
            std::cout << "NO" << std::endl;
            return 0;
        }
    }
    std::cout << "YES" << std::endl;
    return 0;
}