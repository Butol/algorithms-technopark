#include <iostream>
#include <stack>
#include <queue>

template <typename T>
class Tree {
    struct Node {
        Node(const T &data): data(data), left(nullptr), right(nullptr) {};
        int data;
        Node *left, *right;
    };

public:
    Tree(): root(nullptr) {};
    ~Tree() {
        std::stack<struct Node *> stackNodes;
        stackNodes.push(root);

        while(!stackNodes.empty()) {
            struct Node *currentNode = stackNodes.top();
            stackNodes.pop();
            if(currentNode->left)
                stackNodes.push(currentNode->left);
            if(currentNode->right)
                stackNodes.push(currentNode->right);


            delete currentNode;
        }
    }

    void Insert(const T &data) {
        if (!root) {
            root = new Node(data);
            return;
        }

        struct Node *currentNode = root;
        while(true) {
            if(data < currentNode->data) {
                if (!currentNode->left){
                    currentNode->left = new Node(data);
                    break;
                }
                else{
                    currentNode = currentNode->left;
                }
            }
            if(data >= currentNode->data) {
                if (!currentNode->right){
                    currentNode->right = new Node(data);
                    break;
                }
                else{
                    currentNode = currentNode->right;
                }
            }

        }
    }

    int getWidth() {
        if(!root)
            return 0;

        size_t width = 1;
        std::queue<Node*> queue;
        queue.push(root);

        while(!queue.empty()) {
            size_t size = queue.size();
            for(size_t i = 0; i < size; i++) {
                Node *tmp = queue.front();
                if(tmp->left)
                    queue.push(tmp->left);
                if(tmp->right)
                    queue.push(tmp->right);
                queue.pop();
            }
            if (width < queue.size()) {
                width = queue.size();
            }
        }

        return width;
    }

private:
    Node *root;
};

template <typename T>
class Treap {
    struct Node {
        Node(const T &data, const int &priority): data(data), priority(priority),left(nullptr), right(nullptr) {};
        int data, priority;
        Node *left, *right;
    };

public:
    Treap(): root(nullptr) {};
    ~Treap() {
        DeleteNode(root);
    }


    void Insert(const T &data, const int &priority) {
        if (!root) {
            root = new Node(data, priority);
            return;
        }

        bool leftOrRightParent = false;
        Node *currentNode = root;
        Node *parentNode = nullptr;
        while(currentNode && currentNode->priority >= priority) {
            parentNode = currentNode;
            if(data < currentNode->data) {
                    currentNode = currentNode->left;
                    leftOrRightParent = false;
            }
            else {
                    currentNode = currentNode->right;
                    leftOrRightParent = true;
            }
        }

        Node *T1 = nullptr;
        Node *T2 = nullptr;
        split(currentNode, data, T1, T2);
        currentNode = new Node(data,priority);
        currentNode->left = T1;
        currentNode->right = T2;
        if(!parentNode) {
            root = currentNode;
            return;
        }
        if(leftOrRightParent)
            parentNode->right = currentNode;
        else
            parentNode->left = currentNode;
    }

    int getWidth() {
        if(!root)
            return 0;

        size_t width = 1;
        std::queue<Node*> queue;
        queue.push(root);

        while(!queue.empty()) {
            size_t size = queue.size();
            for(size_t i = 0; i < size; i++) {
                Node *tmp = queue.front();
                if(tmp->left)
                    queue.push(tmp->left);
                if(tmp->right)
                    queue.push(tmp->right);
                queue.pop();
            }
            if (width < queue.size()) {
                width = queue.size();
            }
        }

        return width;
    }

private:
    Node *root;

    void DeleteNode(Node* node) {
        if (!node) return;

        DeleteNode(node->left);
        DeleteNode(node->right);
        delete node;
    }

    void split(Node *currentNode, int key, Node *&left, Node *&right) {
        if(!currentNode) {
            left = nullptr;
            right = nullptr;
        } else if (currentNode->data <= key) {
            split(currentNode->right, key, currentNode->right, right);
            left = currentNode;
        } else {
            split( currentNode->left, key, left, currentNode->left);
            right = currentNode;
        }
    }
};

int main() {

    size_t count;
    std::cin >> count;
    Tree<int> tree = Tree<int>();
    Treap<int> treap = Treap<int>();
    int data;
    int priority;

    for (size_t i = 0; i < count; i++) {
        std::cin >> data >> priority;
        tree.Insert(data);
        treap.Insert(data, priority);
    }

    std::cout << treap.getWidth() - tree.getWidth();

    return 0;
}