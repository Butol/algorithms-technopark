#include <iostream>
#include <stack>

template <typename T>
struct Node {
    Node(const T &data): data(data), left(nullptr), right(nullptr) {}
    T data;
    Node* left;
    Node* right;
};

template <typename T>
class Comparator {
public:
    bool operator()(const T &left,const T &right) {
        return left < right;
    }
};

template <typename T, typename Comparator>
class Tree{
public:
    Tree() : root(nullptr) {}
    ~Tree() {
        std::stack<struct Node<int> *> stackNodes;
        stackNodes.push(root);

        while(!stackNodes.empty()) {
            struct Node<int> *currentNode = stackNodes.top();
            stackNodes.pop();
            if(currentNode->left)
                stackNodes.push(currentNode->left);
            if(currentNode->right)
                stackNodes.push(currentNode->right);


            delete currentNode;
        }
    }
    void Insert(const T &data) {
        if (!root) {
            root = new struct Node<T>(data);
            return;
        }

        struct Node<int> *currentNode = root;
        while(true) {
            //if(data < currentNode->data) {
            if(cmp(data,currentNode->data)) {
                 if (!currentNode->left){
                     currentNode->left = new struct Node<T>(data);
                     break;
                 }
                 else{
                     currentNode = currentNode->left;
                 }
            }
            //if(data >= currentNode->data) {
            if(cmp(currentNode->data,data)) {
                if (!currentNode->right){
                    currentNode->right = new struct Node<T>(data);
                    break;
                }
                else{
                    currentNode = currentNode->right;
                }
            }

         }
    }
    void PrintPostOrder() {
        std::stack<struct Node<int> *> stackNodes;
        std::stack<int> stackData;


        stackNodes.push(root);

        while(!stackNodes.empty()) {
            struct Node<int> *currentNode = stackNodes.top();
            stackNodes.pop();

            stackData.push(currentNode->data);

            if(currentNode->left)
                stackNodes.push(currentNode->left);
            if(currentNode->right)
                stackNodes.push(currentNode->right);

        }

        while(!stackData.empty()){
            std::cout << stackData.top() << " ";
            stackData.pop();
        }

    }
private:
    struct Node<T> *root;
    Comparator cmp;
};

int main() {
    int n = 0;
    std::cin >> n;
    Tree<int, Comparator<int>> tree;

    int key;
    std::cin >> key;
    tree.Insert(key);

    for(int i = 0; i < n-1; i++) {
        std::cin >> key;
        tree.Insert(key);
    }

    tree.PrintPostOrder();

    return 0;
}