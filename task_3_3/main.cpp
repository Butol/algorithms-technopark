#include <iostream>
#include <vector>
#include <cassert>
#include <set>
struct IGraph {
    virtual ~IGraph() = default;

    virtual void AddEdge(int from, int to, int weight) = 0;

    virtual std::vector<std::pair<int, int>> GetNextVertices(int vertex) const = 0;
};

class WeightGraph : public IGraph {
public:
    explicit WeightGraph(int size) : adjacencyLists(size), sumWeight(size)
    {
    }

    ~WeightGraph() override = default;

    void AddEdge(int from, int to, int weight) override
    {
        assert(0 <= from && from < adjacencyLists.size());
        assert(0 <= to && to < adjacencyLists.size());

        sumWeight += weight;
        adjacencyLists[from].push_back({to, weight});
        adjacencyLists[to].push_back({from, weight});
    }

    std::vector<std::pair<int, int>> GetNextVertices(int vertex) const override
    {
        assert(0 <= vertex && vertex < adjacencyLists.size());
        std::vector<std::pair<int, int>> nextVertices;

        for(auto& iterator: adjacencyLists[vertex])
        {
            nextVertices.push_back(iterator);
        }
        return nextVertices;
    }

    int LengthMinWay(const int from, const int to)
    {
        assert(0 <= from && from < adjacencyLists.size());
        assert(0 <= to && to < adjacencyLists.size());
        std::vector<int> shortDistance(this->adjacencyLists.size(), sumWeight);
        shortDistance[from] = 0;

        std::set<std::pair<int, int>> pairsQueue;
        pairsQueue.insert({0, from}); // вес ребра _  в вершину _

        while(!pairsQueue.empty()){
            int current = pairsQueue.begin()->second;
            pairsQueue.erase(pairsQueue.begin());
            std::vector<std::pair<int, int>> nextVertices = this->GetNextVertices(current);
            for(auto vertex: nextVertices){
                if (shortDistance[current] + vertex.second < shortDistance[vertex.first])
                {
                    if(shortDistance[vertex.first] != sumWeight)
                        pairsQueue.erase({shortDistance[vertex.first], vertex.first});

                    shortDistance[vertex.first] = shortDistance[current] + vertex.second;
                    pairsQueue.insert({shortDistance[vertex.first], vertex.first});
                }
            }
        }
        return shortDistance[to];
    }

private:
    std::vector<std::vector<std::pair<int, int>>> adjacencyLists;
    int sumWeight; // Сумма всех весов в графе
};

int main() {
    int countCities;
    std::cin >> countCities;
    WeightGraph graph(countCities);
    int countRoads;
    std::cin >> countRoads;
    int from, to, time;
    for (int i = 0; i < countRoads; i++)
    {
        std::cin >> from >> to >> time;
        graph.AddEdge(from, to, time);
    }
    std::cin >> from >> to;
    std::cout << graph.LengthMinWay(from, to) << std::endl;

    return 0;
}

