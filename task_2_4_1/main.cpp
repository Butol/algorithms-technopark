#include <iostream>


template <typename T>
class Comparator {
public:
    bool operator()(const T &left, const T &right) {
        return left < right;
    }
};

template <typename T, typename Comparator>
class AvlTree
{
    struct Node
    {
        Node(const T &data) : data(data), left(nullptr), right(nullptr), height(1), number(1) {}
        T data;
        Node *left, *right;
        size_t height;
        size_t number;
    };

public:
    AvlTree() : root(nullptr) {}
    ~AvlTree()
    {
        destroyTree(root);
    }


    void Add(const T &data, size_t &position)
    {
        root = addInternal(root, data, position);
    }

    void Remove(int index)
    {
        root = removeInternal(root, index);
    }

private:
    Node *root;
    Comparator cmp;

    void fixHeight(Node *node)
    {
        node->height = 1 + std::max(getHeight(node->left), getHeight(node->right));
    }

    void fixNumber(Node* node ) {
        node->number = 1 + getNumber( node->left ) + getNumber( node->right );
    }
    
    size_t getHeight(Node *node)
    {
        return node ? node->height : 0;
    }

    size_t getNumber(Node* node ) {
        return node ? node->number : 0;
    }

    int getBalance(Node *node)
    {
        return getHeight(node->right) - getHeight(node->left);
    }

    Node* rotateLeft(Node *node)
    {
        Node *tmp = node->right;
        node->right = tmp->left;
        tmp->left = node;

        fixHeight(node);
        fixHeight(tmp);
        fixNumber(node);
        fixNumber(tmp);

        return tmp;
    }

    Node* rotateRight(Node *node)
    {
        Node *tmp = node->left;
        node->left = tmp->right;
        tmp->right = node;

        fixHeight(node);
        fixHeight(tmp);
        fixNumber(node);
        fixNumber(tmp);

        return tmp;
    }

    Node* doBalance(Node *node)
    {
        if (!node)
            return nullptr;
        fixHeight(node);
        fixNumber(node);

        switch (getBalance(node))
        {
            case 2:
            {
                if (getBalance(node->right) < 0)
                    node->right = rotateRight(node->right);
                return rotateLeft(node);
            }
            case -2:
            {
                if (getBalance(node->left) > 0)
                    node->left = rotateLeft(node->left);
                return rotateRight(node);
            }
            default:
                return node;
        }
    }

    Node* findMin(Node *node)
    {
        while (node->left)
            node = node->left;
        return node;
    }

    Node* findMax(Node *node)
    {
        while (node->right)
            node = node->right;
        return node;
    }

    Node* addInternal(Node *node, const T &data, size_t &position)
    {
        if (!node)
            return new Node(data);
        if(cmp(node->data, data))
            node->right = addInternal(node->right, data, position);
        else
            if(cmp(data, node->data)) {
                position += getNumber(node->right) + 1;
                node->left = addInternal(node->left, data, position);
            }
        return doBalance(node);
    }

    Node* removeInternal(Node *node, int position)
    {
        if (!node || position > getNumber(node))
            return nullptr;
        int currentPosition = getNumber(node->right) + 1;
        if (position < currentPosition)
            node->right = removeInternal(node->right, position);
        else if (position > currentPosition)
            node->left = removeInternal(node->left, position - currentPosition);

        else {
            Node* left = node->left;
            Node* right = node->right;


            if(!right) {
                delete node;
                return left;
            }
            if(!left) {
                delete node;
                return right;
            }

            bool flag = right->height > left->height;
            Node *tmp = nullptr;
            if (flag) {
                tmp = findMin(right);
                node->data = tmp->data;
                node->right = removeInternal(node->right, node->right->number);
                node->left = left;
            } else {
                tmp = findMax(left);
                node->data = tmp->data;
                node->left = removeInternal(node->left, 1);
                node->right = right;
            }
            return doBalance(node);
        }

        return doBalance(node);
    }

    void destroyTree(Node *node)
    {
        if (node)
        {
            destroyTree(node->left);
            destroyTree(node->right);
            delete node;
        }
    }
};

int main(int argc, const char * argv[]) {
    AvlTree<int, Comparator<int>> avlTree;

    int count = 0;
    int operation = 0;
    int parameter = 0;
    std::cin >> count;
    for(int i = 0; i < count; i++) {
        std::cin >> operation >> parameter;
        switch (operation)
        {
            case 1:
            {
                size_t position = 0;
                avlTree.Add(parameter, position);
                std::cout << position << std::endl;
                break;
            }
            case 2:
            {
                parameter++;
                avlTree.Remove(parameter);
                break;
            }
        }
    }
    return 0;
}
