/*
 * Бутолин Александр АПО-13 Задача 2 Вариант 2
 *
 * Дан массив целых чисел А[0..n-1].
 * Известно, что на интервале [0, m] значения массива строго возрастают, а на интервале [m, n-1] строго убывают.
 * Найти m за O(log m).
 *
 */

#include <iostream>

int BinarySearch(const int* arr, int start, int end); // Функция бинарного поиска
void InitArray(int* arr, int count); // инициализация массив
void PrintArray(const int* arr, int count); // Печать масива
int SearchInterval(const int* arr, const int* size); // Поиск интервала смены убывания массива

int main() {

    int size;
    std::cin >> size;
    int* arr = new int[size];

    InitArray(arr, size);
    //PrintArray(arr, size);

    std::cout << SearchInterval(arr, &size);

    delete [] arr;

    return 0;
}

int BinarySearch(const int* arr, int start, int end) {

    while(start < end) {
        int mid = (start + end) / 2;

        // 1 < 3 > 2
        if (arr[mid-1] < arr[mid] && arr[mid] > arr[mid + 1])
            return mid;

        // 3 > 2 > 1
        else if (arr[mid - 1] > arr[mid] && arr[mid] > arr[mid + 1])
            end = mid;

        // 1 < 2 < 3
        else if (arr[mid - 1] < arr[mid] && arr[mid] < arr[mid + 1])
            start = mid + 1;
        else
            return -1;

    }

    return -1;

}

void InitArray(int* arr, int count) {

    for (long i = 0; i < count; i++) {
        std::cin >> arr[i];
    }
}

void PrintArray(const int* arr, int count) {

    for (int i = 0; i < count; i++) {
        std::cout << arr[i] << " ";
    }
    std::cout << std::endl;
}

int SearchInterval(const int* arr, const int* size)
{
    // Проверка первого элемента
    if (arr[0] > arr[1])
            return 0;

    // Проверка последнего элемента
    if (arr[*size - 2] < arr[*size - 1])
        return *size - 1;

//    // Случай с массивом из 2-х элементов
//    if (arr[0] < arr[1] && *size == 2)
//            return 1;

    // Проверка предпоследнего элемента
    if (arr[*size - 1] < arr[*size - 2] && (arr[*size - 3] < arr[*size - 2]))
        return *size - 2;


    // Счетчики с помощью которых будем бежать по массиву(являются индексами массива)
    int start = 1;
    int end = 2; // 2 потому что массив из 2-х элементов обработан выше
    int result = 0;

    while(end + 1 <= *size) {
        result = BinarySearch(arr, start, end);

        if (result != -1)
            return result;

        start *= 2;

        if (end * 2 >= *size)
            end = *size - 1;
        else
            end *= 2;

    }

    return *size - 1 ; // Случай если весь массив строго возрастающий
}
