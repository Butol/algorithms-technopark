/*
 * Бутолин Александр АПО-13 Задача 1 Вариант 1
 *
 * Даны два массива целых чисел одинаковой длины A[0..n-1] и B[0..n-1].
 * Необходимо найти первую пару индексов i0 и j0, i0 ≤ j0, такую что
 * A[i0] + B[j0] = max {A[i] + B[j], где 0 <= i < n, 0 <= j < n, i <= j}.
 *
 */

#include <iostream>

void InitArray(int* arr, int count);
void PrintArray(const int* arr, int count);
void SearchMaxSum(int* arr1, int* arr2, int* a, int* b, int size);

int main()
{
    int size;

    std::cin >> size;

    int * arr1 = new int[size];
    int * arr2 = new int[size];

    InitArray(arr1, size);
    InitArray(arr2, size);

    int a = 0; //Индекс массива arr1
    int b = 0; //Индекс массива arr2

    SearchMaxSum(arr1, arr2, &a, &b, size);

    std::cout << a << " " << b;

    delete [] arr1;
    delete [] arr2;

    return 0;
}

void InitArray(int* arr, int count) {

    for (long i = 0; i < count; i++) {
        std::cin >> arr[i];
    }
}

void PrintArray(const int* arr, int count) {

    for (int i = 0; i < count; i++) {
        std::cout << arr[i] << " ";
    }
    std::cout << std::endl;
}

void SearchMaxSum(int* arr1, int* arr2, int* a, int* b, int size)
{
    int maxSum = arr1[*a] + arr2[*b];
    int maxArr1 = 0; // индекс максимального элемента первого массива

    for (int i = 1; i < size; i++) {

        if (arr1[i] > arr1[maxArr1]) {
            maxArr1 = i;
        }

        if (arr2[i] + arr1[maxArr1] > maxSum)
        {
            maxSum = arr2[i] + arr1[maxArr1];
            *a = maxArr1;
            *b = i;
        }

    }

}