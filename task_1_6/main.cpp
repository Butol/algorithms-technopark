#include <iostream>

template  <typename T>
int take_partition_element(T* arr, int l, int r){
    int half = (l + r) / 2;
    if ( ( (l > half) && (l < r) ) || ( (l < half) && (l > r) ) )
        return l;
    else
        if ( ( (r > half) && (r < l) ) ||  ( (r < half) && (r > l) ) )
            return r;
        else
            return half;

}

template  <typename T, class Compare>
int partition(T* arr, int l, int r, Compare cmp){
    int pivot_position = take_partition_element(arr, l, r);

    if (pivot_position != r)
        std::swap(arr[pivot_position], arr[r]);

    int i = l;
    int j = l;

    while(j != r) {
        if (cmp(arr[j],arr[r])) {
            std::swap(arr[i], arr[j]);
            i++;
            j++;
        } else
            j++;
    }

    std::swap(arr[r], arr[i]);

    return i;
}

template  <typename T, class Compare>
T kth_statistic(T* arr, int l, int r, int k, Compare cmp){

    int partition_pos = 0;

    while(l <= r) {
        partition_pos = partition(arr, l, r, cmp);
        if (partition_pos == k)
            return arr[partition_pos];
        if (k > partition_pos)
            l = partition_pos + 1;
        if (k < partition_pos)
            r = partition_pos - 1;
    }

    return arr[partition_pos];
}


int main() {

    int n = 0;
    std::cin >> n;
    int k = 0;
    std::cin >> k;

    int* arr = new int[n];
    for(int i = 0; i < n; i++){
        std::cin >> arr[i];
    }

    std::cout << kth_statistic(arr, 0, n - 1, k, [](const int& l, const int& r)  {return l < r;});

    delete[] arr;

    return 0;
}